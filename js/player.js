function LePlayer(params) {
    var that = this;
    var userAgent = window.navigator.userAgent;
    var isAndroid = /android/i.test(userAgent) ? true : false; //android终端
    var isiOS = /(iPhone|iPad|iPod|iOS)/i.test(userAgent) ? true : false; //ios终端
    var playerWrapper = document.getElementById(params.wrapperId);
    var clientHeight = document.documentElement.clientHeight;
    var screenWidth = window.screen.width;
    var screenHeight = window.screen.height;
    var headerHeight = screenHeight - clientHeight;
    var urlsLength = params.urls.length;
    var currentUrl = 0;
    playerWrapper.innerHTML = '<header id="header" class="header">' + params.title + '</header>'
                                + '<div id="le-player" class="le-player">'
                                +   '<video id="le-video" class="le-video" preload="preload" playsinline webkit-playsinline '
                                +     'x5-video-player-type="h5" x5-video-orientation="portrait" x5-video-player-fullscreen="true" '
                                +     'loop poster="../images/poster.jpg">'
                                +     '<source src=' + params.urls[currentUrl] + '>您的浏览器暂不支持video标签'
                                +   '</video>'
                                +   '<ul id="controls" class="controls clear">'
                                +     '<li>'
                                +       '<img id="play-button" class="le-icons" src="./images/icons/play.png" alt="">'
                                +     '</li>'
                                +     '<li class="not-live">'
                                +       '<img id="backword-button" class="le-icons" src="./images/icons/backward.png" alt="">'
                                +     '</li>'
                                +     '<li class="not-live">'
                                +       '<img id="forward-button" class="le-icons" src="./images/icons/forward.png" alt="">'
                                +     '</li>'
                                +     '<li class="time-show not-live">'
                                +       '<span id="current-time">00:00</span>'
                                +     '</li>'
                                +     '<li id="time-bar">'
                                +       '<p class="is-live" style="color:#fff;margin-top:-5px;">直播中</p>'
                                +       '<p id="time-bar-line" class="not-live"></p>'
                                +       '<p id="time-bar-lined" class="not-live"></p>'
                                +       '<div id="time-bar-control" class="not-live"></div>'
                                +     '</li>'
                                +     '<li class="time-show not-live">'
                                +       '<span id="duration">00:00</span>'
                                +     '</li>'
                                +     '<li id="set-muted">'
                                +       '<img id="sound-button" class="le-icons" src="./images/icons/sound-on.png" alt="">'
                                +     '</li>'
                                +     '<li id="screen-option">'
                                +       '<img id="full-screen" class="le-icons" src="./images/icons/full-screen.png" alt="">'
                                +       '<img id="video-orientation" class="le-icons" src="./images/icons/landscape.png" alt="">'
                                +     '</li>'
                                +   '</ul>'
                                +   '<img id="load-img" src="./images/loading.jpg" alt="">'
                                +   '<div id="tab-tip">切换至</div>'
                                + '</div>'
                                + '<canvas id="print-screen-box" width="270" height="135" style="display:none;">'
                                +   '您的浏览器暂不支持canvas标签'
                                + '</canvas>';
    params.urls.forEach(function(value,index){
        $('#tab-tip')[0].innerHTML += '<span style="color:#ccc"> 线路'+ ( index + 1 ) + ' </span>';
    });
    var playerHeader = $('#header');
    var player = $('#le-video');
    var playerBox = $('#le-player');
    var playerControls = $('#controls');
    var playerButtons = $('#play-button');
    var backwordButton = $('#backword-button');
    var forwardButton = $('#forward-button');
    var soundButton = $('#sound-button');
    var videoDuration = $('#duration');
    var videoCurrentTime = $('#current-time');
    var isPlay = false;
    var isSound = false;
    var timeBar = $('#time-bar');
    var timeBarControl = $('#time-bar-control');
    var currentWidth;
    var fullScreenButton = $('#full-screen');
    var isFullScreen = false;
    var printScreenBox = $('#print-screen-box');
    var timerControls;
    var timerTab;
    // var watingTimes = 0;
    // 初始化
    this.init = function(){
        that.player = player;
    }();
    // 阻止默认事件
    this.preventDefaultEvent = function(e) {
        e.preventDefault();
    };
    // 时间格式方法
    this.dateFormat = function (time) {
        var intTime = Math.floor(time);
        var minutes = parseInt(intTime / 60);
        minutes < 10 ? minutes = '0' + minutes : minutes = minutes;
        var seconds = intTime % 60;
        seconds < 10 ? seconds = '0' + seconds : seconds = seconds;
        return  minutes + ':' + seconds;
    };
    // 播放时间轴控制
    this.timeLineControl = function(){
        timeBar.on('touchend',function(e){
            var currentTouches = e.originalEvent.changedTouches[0].clientX - e.delegateTarget.offsetLeft;
            timeBarControl.css('left', currentTouches);
            $('#time-bar-lined').width(currentTouches);
            player[0].currentTime = (currentTouches / timeBar.width()) * player[0].duration;
        });
        timeBarControl[0].addEventListener("touchstart", function (e) {
            var touches = e.touches[0];
            currentWidth = touches.clientX - timeBarControl[0].offsetLeft - timeBarControl.width() / 2;
            //阻止页面的滑动默认事件
            document.addEventListener("touchmove", that.preventDefaultEvent, false);
        }, false);
        timeBarControl[0].addEventListener("touchmove", function (e) {
            var touches = e.touches[0];
            var leftOffset = touches.clientX - currentWidth;
            leftOffset < 0 ? leftOffset = 0 : (leftOffset > timeBar[0].clientWidth - timeBarControl[0].offsetWidth
                ? leftOffset = (timeBar[0].clientWidth - timeBarControl[0].offsetWidth) : leftOffset = leftOffset);
            timeBarControl.css('left', leftOffset);
            $('#time-bar-lined').width(leftOffset);
            player[0].currentTime = (leftOffset / timeBar.width()) * player[0].duration;
        }, false);
        timeBarControl[0].addEventListener("touchend", function () {
            document.removeEventListener("touchmove", that.preventDefaultEvent, false);
        }, false);
    };
    // 全屏方法
    this.fullScreen = function (isFullScreen) {
        if (isiOS) {
            // Safari 5.0 && iOS
            player[0].webkitEnterFullScreen();
        }
        if (isAndroid) {
            if (isFullScreen) {
                $('#full-screen').hide();
                $('#video-orientation').show()
                .attr('src','./images/icons/landscape.png');
                playerBox.css('height', screenHeight);
                player.css('object-position', 'center center')
                .parents('body').removeClass('fullscreen');
            } else {
                $('#video-orientation').hide();
                $('#full-screen').show();
                if (player.attr('x5-video-orientation') == 'landscape') {
                    player.attr('x5-video-orientation', 'portrait');
                    playerBox.css('height', screenWidth - headerHeight);
                    playerControls.css('width', screenWidth);
                } else {
                    playerBox.css('height', screenWidth - headerHeight);
                }
                player.css('object-position', 'center ' + headerHeight + 'px')
                .parents('body').addClass('fullscreen');
            }
        }
    };
    // 截图转 base64 编码字符方法
    this.printScreen = function(){
        var baseUrl = printScreenBox[0].toDataURL() + '_timestamp' + that.dateFormat(new Date().getTime());
        return baseUrl;
    };
    // 屏幕尺寸调整
    window.onresize = function () {
        player.css({
            'width': window.innerWidth,
            'height': window.innerHeight,
        });
        playerControls.css('width', window.innerWidth);
    };
    // 判断是否直播 && 初始化头部标题文字居中和播放器盒子高度
    if(params.isLive){
        $('.not-live').hide();
        $('.is-live').show();
    };
    !(params.isLive) && this.timeLineControl();
    playerHeader.css('line-height', headerHeight + 'px');
    playerBox.css('height', screenWidth - headerHeight * 2);
    // IOS取消静音设置
    if(isiOS) {
        $('#set-muted').hide();
        $('#screen-option').show();
    }
    // 腾讯X5 && 取消视频默认的点击事件
    player[0].addEventListener("touchend", that.preventDefaultEvent, false);
    if (isAndroid) {
        player.on('x5videoenterfullscreen', function () {
            $('#screen-option').show();
            playerHeader.show();
            $(this).css({
                'object-position': 'center ' + headerHeight + 'px'
            })
            .parents('body').addClass('fullscreen');
            playerBox.css('height', screenWidth - headerHeight);
        })
        .on('x5videoexitfullscreen', function () {
            $('#screen-option').hide();
            playerControls.stop().fadeIn(500);
            playerHeader.hide();
            $('#video-orientation').hide();
            $('#full-screen').show();
            playerBox.css('height', screenWidth - headerHeight * 2);
            player.attr('x5-video-orientation', 'portrait').css({
                'width': '100%',
                'height': '100%',
                'object-position': 'center top'
            })
            .parents('body')
            .removeClass('fullscreen');
        });
    }
    // 播放 && 暂停
    playerBox.on('touchend',function(){
        clearTimeout(timerControls);
        playerControls.stop().fadeIn(500);
        timerControls = setTimeout(function(){
            playerControls.stop().fadeOut(500);
        },3000);
    });
    playerButtons.one('touchstart',function(){
        setTimeout(function(){
            playerControls.fadeOut(500);
        },3000);
    })
    .on('touchend', function () {
        isPlay = !isPlay;
        isPlay ? player[0].play() : player[0].pause();
    });
    // 快进 && 快退
    backwordButton.on('touchend',function(){
        player[0].currentTime > 5 ? player[0].currentTime -= 5 : player[0].currentTime = 0;
    });
    forwardButton.on('touchend',function(){
        player[0].duration - player[0].currentTime < 5 ? player[0].currentTime = player[0].duration - 2 : player[0].currentTime += 5;
    });
    // 当前（总）时间显示 && 加载动画
    player.on('timeupdate', function () {
        videoCurrentTime.html(that.dateFormat(this.currentTime));
        videoDuration.html(that.dateFormat(player[0].duration));
        var currentPosition = (this.currentTime / this.duration) * timeBar.width();
        timeBarControl.css('left', currentPosition);
        $('#time-bar-lined').width(currentPosition);
    })
    .on('waiting', function () {
        isiOS && $('#load-img').show();
        timerTab = setTimeout(function(){
            isiOS && $('#tab-tip').show();
        },3000);
        // isiOS && watingTimes >= 5 ? $('#tab-tip').show() : watingTimes ++;
    })
    .on('loadstart',function(){
        isAndroid && $('#load-img').show();
        timerTab = setTimeout(function(){
            isAndroid && $('#tab-tip').show();
        },10000);
        // isAndroid && watingTimes >= 5 ? $('#tab-tip').show() : watingTimes ++;
    })
    .on('canplay', function () {
        $('#load-img').hide();
        clearTimeout(timerTab);
        // watingTimes >= 5 ? $('#tab-tip').show() : $('#tab-tip').hide();
    })
    .on('play',function(){
        playerButtons[0].src = './images/icons/pause.png';
    })
    .on('pause',function(){
        playerButtons[0].src = './images/icons/play.png';
    });
    // 切换线路
    ;(params.isLive) && $('#tab-tip span').on('touchend',function(){
        var index = $(this).index();
        currentUrl = index;
        var currentTime = player[0].currentTime;
        player[0].src = params.urls[currentUrl];
        player[0].load();
        player[0].currentTime = currentTime;
        player[0].play();
    });
    // 静音
    soundButton.on('touchend', function () {
        isSound = !isSound;
        player[0].muted = !isSound;
        isSound ? this.src = './images/icons/sound-on.png' : this.src = './images/icons/sound-off.png';
    });
    // 全屏操作
    fullScreenButton.on('touchend', function () {
        isFullScreen = !isFullScreen;
        that.fullScreen(isFullScreen);
    });
    // 横竖屏切换
    $('#video-orientation').on('touchend', function () {
        if (player.attr('x5-video-orientation') == 'portrait') {
            $(this).attr('src', './images/icons/portrait.png');
            player.attr('x5-video-orientation', 'landscape');
            playerBox.css('height', screenWidth);
        } else if (player.attr('x5-video-orientation') == 'landscape') {
            $(this).attr('src', './images/icons/landscape.png');
            player.attr('x5-video-orientation', 'portrait').css('object-position', 'center center');
            playerBox.css('height', screenHeight)
            .parents('body').removeClass('fullscreen');
        }
    });
}
LePlayer.prototype.getMetaData = function(callBack){
    var player = this.player;
    player.on('loadedmetadata',function(){
        var data = {
            width: player[0].videoWidth,
            height: player[0].videoHeight,
            duration: player[0].duration
        };
        callBack(data);
    });
}
LePlayer.prototype.listenStatus = function(eventType,callBack){
    var player = this.player;
    player.on('play',function(){
        eventType == 'play' && callBack();
    }).on('pause',function(){
        eventType == 'pause' && callBack();
    });
}